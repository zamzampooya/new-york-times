//
//  UIImageViewExtension.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation
import SDWebImage

extension UIImageView {
    /**
     This is a wrapper for SDWebImage in order to make it easy to use and easy to modify.
     
     - Parameter urlString: The Image Url.
     - Parameter placeholder: The placeholder which is shown before loading the image
     - Parameter completionHandler: A closure which is called with a boolean which shows whether Image is loaded or not
     */
    func setImage(urlString: String?, placeholder: UIImage? = nil, completionHandler: ((Bool) -> ())? = nil) {
        if let urlString = urlString {
            let url = URL(string: urlString)
            self.sd_setImage(with: url, placeholderImage: placeholder, options: .progressiveDownload, progress: nil) { (_, _, _, _) in
                completionHandler?(true)
                
            }
        } else if let placeholder = placeholder {
            self.image = placeholder
        }
    }
}
