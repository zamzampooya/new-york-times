//
//  SeeMoreWebViewViewController.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/18/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import UIKit
import WebKit

class SeeMoreWebViewViewController: UIViewController {
    // MARK: - Private Variables  -
    private var webView: WKWebView!
    private var seeMore: String?
    
    // MARK: - Overrides  -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpWebView()
    }
    
}
// MARK: - private function  -
extension SeeMoreWebViewViewController {
    func setUpWebView() {
        if let seeMore = self.seeMore,
            let url = URL(string: seeMore) {
            webView.load(URLRequest(url: url))
        }
        
    }
}
// MARK: - Public function  -
extension SeeMoreWebViewViewController {
    public func setContent(seeMore: String) {
        self.seeMore = seeMore
    }
}
// MARK: - WKNavigationDelegate  -
extension SeeMoreWebViewViewController: WKNavigationDelegate {
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
}
