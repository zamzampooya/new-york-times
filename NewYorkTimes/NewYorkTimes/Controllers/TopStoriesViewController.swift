//
//  TopStoriesViewController.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import UIKit

class TopStoriesViewController: UIViewController {
    
    // MARK: - Constant Variables  -
    let pageTitle = "Top Stories"
    let heightForRow: CGFloat = 80
    let numberOfSections: Int = 1
    let topStoriesTableViewCellIdentifier = "TopStoriesTableViewCell"
    let alerControllerTitle = "Error"
    let alertControllerMessage = "Something went wrong, please try again"
    let action1Title = "Ok"
    let action2Title = "Cancel"
    
    // MARK: - IBOutlets  -
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var topStoriesTableView: UITableView! {
        didSet {
            self.topStoriesTableView.register(UINib(nibName: self.topStoriesTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: self.topStoriesTableViewCellIdentifier)
            self.topStoriesTableView.delegate = self
            self.topStoriesTableView.dataSource = self
        }
    }
    
    // MARK: - Variables -
    private var presenter: TopStoriesPresenter?
    private var topStoriesViewModel = [TopStoryViewModel]() {
        didSet {
            self.topStoriesTableView.reloadData()
        }
    }
    // MARK: - Override -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenter()
        self.setTitle()
        self.getTopStoriesViewModel()
        
    }

}
// MARK: - setPresenter  -
extension TopStoriesViewController {
    private func setPresenter() {
        self.presenter = TopStoriesPresenter(delegate: self, network: TopStoriesNetwork())
    }
}
// MARK: - SetupView -
extension TopStoriesViewController {
    private func setTitle() {
        self.title = self.pageTitle
    }
}
// MARK: - getTopStories -
extension TopStoriesViewController {
    private func getTopStoriesViewModel() {
        self.presenter?.getTopStories()
    }
}
// MARK: - UITableViewDataSource  -
extension TopStoriesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.numberOfSections
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.topStoriesViewModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.topStoriesTableViewCellIdentifier, for: indexPath) as! TopStoriesTableViewCell
       cell.setContent(topStoryViewModel: self.topStoriesViewModel[indexPath.row])
        return cell
    }
}
// MARK: - UITableViewDelegate  -
extension TopStoriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heightForRow
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.goToTopStoryDetail(controller: self, withData: self.topStoriesViewModel[indexPath.row])
    }
}
// MARK: - TopStoriesPresenterInterface  -
extension TopStoriesViewController: TopStoriesPresenterInterface {
    func startLoading() {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
    }
    
    func setTopStories(topStories: [TopStoryViewModel]) {
        self.topStoriesViewModel = topStories
    }
    
    func handleError() {
        let alertController = UIAlertController(title: self.alerControllerTitle, message: self.alertControllerMessage, preferredStyle: .alert)
        let action1 = UIAlertAction(title: self.action1Title, style: .default) { (action:UIAlertAction) in
            self.getTopStoriesViewModel()
        }
        let action2 = UIAlertAction(title: self.action2Title, style: .cancel, handler: nil)
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.popoverPresentationController?.sourceView = self.view
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}
