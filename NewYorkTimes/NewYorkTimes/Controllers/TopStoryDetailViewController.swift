//
//  TopStoryDetailViewController.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import UIKit

class TopStoryDetailViewController: UIViewController {
    // MARK: - Constant Variables  -
    let estimatedRowHeight: CGFloat = 40
    let section0HeaderHeight: CGFloat = 0
    let section1HeaderHeight: CGFloat = 80
    let numberOfSections: Int = 2
    let numberOfSection0Rows: Int = 1
    let numberOfSection1Rows: Int = 2
    let authorTableViewCellIdentifier = "AuthorTableViewCell"
    let descriptionTableViewCellIdentifier = "DescriptionTableViewCell"
    let largeImageTableViewCellIdentifier = "LargeImageTableViewCell"
    let headerTitleTableViewCellIdentifier = "HeaderTitleTableViewCell"
    
    // MARK: - Computed Property  -
    var imageRowHeight: CGFloat {
       return (UIScreen.main.bounds.width * (9/16))
    }
    // MARK: - IBOutlets  -
    @IBOutlet weak var detailTableView: UITableView! {
        didSet {
            // MARK: registering Nibs
            self.detailTableView.register(UINib(nibName: self.authorTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: self.authorTableViewCellIdentifier)
            self.detailTableView.register(UINib(nibName: self.descriptionTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: self.descriptionTableViewCellIdentifier)
            self.detailTableView.register(UINib(nibName: self.largeImageTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: self.largeImageTableViewCellIdentifier)
            self.detailTableView.register(UINib(nibName: self.headerTitleTableViewCellIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: self.headerTitleTableViewCellIdentifier)
            // MARK: tableView Setting
            self.detailTableView.estimatedRowHeight = self.estimatedRowHeight
            self.detailTableView.rowHeight = UITableView.automaticDimension
            self.detailTableView.delegate = self
            self.detailTableView.dataSource = self
            
        }
    }
    // MARK: - Varibales  -
    private var topStoryViewModel: TopStoryViewModel?
    private var presenter = TopStoryDetailPresenter()

    // MARK: - Override  -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle()
        self.detailTableView.reloadData()
    }
    
}

// MARK: - Public Functions  -
extension TopStoryDetailViewController {
    public func setContent(topStoryViewModel: TopStoryViewModel) {
        self.topStoryViewModel = topStoryViewModel
    }
}
// MARK: - private functions  -
extension TopStoryDetailViewController {
    private func setTitle() {
        self.title = self.topStoryViewModel?.navigationBarTitle
    }
}
// MARK: - UITableViewDataSource  -
extension TopStoryDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.numberOfSections
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return numberOfSection0Rows
        default:
            return numberOfSection1Rows
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: self.largeImageTableViewCellIdentifier, for: indexPath) as! LargeImageTableViewCell
            cell.setContent(largeImage: self.topStoryViewModel?.largeImage ?? "")
            return cell
        default:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: self.descriptionTableViewCellIdentifier, for: indexPath) as! DescriptionTableViewCell
                cell.setContent(description: self.topStoryViewModel?.description ?? "")
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: self.authorTableViewCellIdentifier, for: indexPath) as! AuthorTableViewCell
                cell.setContent(author: self.topStoryViewModel?.author ?? "", seeMore: self.topStoryViewModel?.seeMore ?? "")
                cell.delegate = self
                return cell
            }
        }
    }
    
}
// MARK: - UITableViewDelegate  -
extension TopStoryDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return self.imageRowHeight
        default:
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return self.section0HeaderHeight
        default:
            return self.section1HeaderHeight
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: self.headerTitleTableViewCellIdentifier) as! HeaderTitleTableViewCell
        headerView.setContent(headerTitle: self.topStoryViewModel?.title ?? "")
        return headerView
    }
}

// MARK: - SeeMoreDelegate  -
extension TopStoryDetailViewController: SeeMoreDelegate {
    func userDidSelectSeeMore(seeMore: String) {
        self.presenter.goToWebView(controller: self, withUrl: seeMore)
    }
}
