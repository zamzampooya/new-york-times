//
//  MultiMediaModel.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation
/**
**MultiMediaModel** is recieved from server. It contains the url for Images.
 */
class MultiMediaModel: Codable {
    var url: String?
    var format: String?
    var height: Int?
    var width: Int?
    var type: String?
    var subtype: String?
    var caption: String?
    var copyright: String?
}
