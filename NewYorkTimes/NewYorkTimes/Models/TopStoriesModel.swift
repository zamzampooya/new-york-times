//
//  TopStoriesModel.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation
/**
 Model recieved from the server.
 
 ## Important Notes ##
 1. Has an array of **TopStoryModel**.
 2. **TopStoryModel** is used to make the **TopStoryViewModel**. 
 */
class TopStoriesModel: Codable {
    
    var status: String?
    var copyright: String?
    var section: String?
    var lastUpdated: String?
    var numResults: Int?
    var results: [TopStoryModel]?
    enum CodingKeys:String,CodingKey
    {
        case status
        case copyright
        case section
        case lastUpdated = "last_updated"
        case numResults = "num_results"
        case results
        
    }
}


