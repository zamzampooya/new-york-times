//
//  TopStoryModel.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation
/**
 **TopStoryModel** is recieved from server and is later used to make the **TopStoryViewModel**
 */
class TopStoryModel: Codable {
    var section: String?
    var subsection: String?
    var title: String?
    var abstract: String?
    var url: String?
    var byline: String?
    var itemType: String?
    var updatedDate: String?
    var createdDate: String?
    var publishedDate: String?
    var materialTypeFacet: String?
    var kicker: String?
    var multimedia: [MultiMediaModel]?
    enum CodingKeys:String,CodingKey
    {
        case section
        case subsection
        case title
        case abstract
        case url
        case byline
        case itemType = "item_type"
        case updatedDate = "updated_date"
        case createdDate = "created_date"
        case publishedDate = "published_date"
        case materialTypeFacet = "material_type_facet"
        case kicker
        case multimedia
        
    }
}

