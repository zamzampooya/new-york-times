//
//  TopStoryDetailPresenter.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation

class TopStoryDetailPresenter {
    /**
     This method is called from view to navigate to **SeeMoreWebViewViewController**.
     - Parameter controller: the viewController which is of kind **TopStoryDetailViewController** responsible of pushing the instance of **SeeMoreWebViewViewController**.
     - Parameter seeMore: the initial data needed to call the **createSeeMoreWebViewViewController** method of **TopStoryConfigurator** and make the instance of **SeeMoreWebViewViewController**.
     */
    public func goToWebView(controller: TopStoryDetailViewController,withUrl seeMore: String) {
        let seeMoreWebViewViewController = TopStoryConfigurator.createSeeMoreWebViewViewController(withData: seeMore)
        controller.navigationController?.pushViewController(seeMoreWebViewViewController, animated: true)

    }
    
}
