//
//  TopStoriesPresenter.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation
/**
 This protocol is the Interface of TopStoriesPresenter.
 
 ## It has the Following functions ##
 1. **startLoading**
 2. **stopLoading**
 3. **setTopStories**
 4. **handleError**
 */
protocol TopStoriesPresenterInterface: class {
    /// **startLoading** which is to notify the view to start loading
    func startLoading()
    /// **stopLoading** which is to notify the view to stop loading
    func stopLoading()
    /// **setTopStories** which is to set the **topStories** of the view.
    func setTopStories(topStories: [TopStoryViewModel])
    /// **handleError** which is to notify the view that it should handle the error.
    func handleError()
}

class TopStoriesPresenter {
    // MARK: - Variables  -
    let network: TopStoriesNetwork
    private weak var delegate: TopStoriesPresenterInterface?
    // MARK: - init  -
    init(delegate: TopStoriesPresenterInterface, network:TopStoriesNetwork) {
        self.delegate = delegate
        self.network = network
    }
    
}
// MARK: - Public Functions  -
extension TopStoriesPresenter {
    /**
     This method is called from view to get the **topStoriesModel** from network and making the **topStoriesViewModel** to send to view.
     */
    public func getTopStories() {
        self.delegate?.startLoading()
        self.network.getTopStories { (topStoriesModel) in
            if let topStoriesModel = topStoriesModel, let results = topStoriesModel.results, results.count > 0 {
                let topStoriesViewModel: [TopStoryViewModel] = self.makeTopStoriesViewModel(topStoriesModel: results)
                self.delegate?.setTopStories(topStories: topStoriesViewModel)
                self.delegate?.stopLoading()
            } else {
                self.delegate?.handleError()
                self.delegate?.stopLoading()
            }
        }
        
    }
    
    /**
     This method is called from view to navigate to **TopStoryDetailViewController**.
     - Parameter controller: the viewController which is of kind **TopStoriesViewController** and is responsible of pushing the instance of **TopStoryDetailViewController**.
     - Parameter data: the initial data needed to call the **createTopStoryDetailViewController** method of **TopStoryConfigurator** and make the instance of **TopStoryDetailViewController**.
     */
    public func goToTopStoryDetail(controller: TopStoriesViewController, withData data: TopStoryViewModel) {
        let topStoryDetailViewController = TopStoryConfigurator.createTopStoryDetailViewController(withData: data)
        controller.navigationController?.pushViewController(topStoryDetailViewController, animated: true)
    }
}

// MARK: - Private Functions  -
extension TopStoriesPresenter {
    /**
     This method makes the **[TopStoryViewModel]***.
     
     - Parameter topStoriesModel: an array of original model fetched from server data.
     - returns: [TopStoryViewModel] an array of **TopStoryViewModel** to be given to view.
     */
    private func makeTopStoriesViewModel(topStoriesModel: [TopStoryModel]) -> [TopStoryViewModel] {
        var topStoriesViewModel:[TopStoryViewModel] = []
        for topStoryModel in topStoriesModel {
            let topStoryViewModel = TopStoryViewModel(topStoryModel: topStoryModel)
            topStoriesViewModel.append(topStoryViewModel)
        }
        return topStoriesViewModel
    }
}
