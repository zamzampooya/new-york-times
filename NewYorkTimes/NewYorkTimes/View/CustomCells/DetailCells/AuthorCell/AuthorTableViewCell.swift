//
//  AuthorTableViewCell.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import UIKit
protocol SeeMoreDelegate: class {
    func userDidSelectSeeMore(seeMore: String)
}
class AuthorTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets  -
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var seeMoreButton: UIButton!
    
    // MARK: - Variables  -
    private var seeMore: String = ""
    weak var delegate: SeeMoreDelegate?
    
    // MARK: - Overrides  -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // MARK: - IBActions  -
    @IBAction func seeMore(_ sender: Any) {

        self.delegate?.userDidSelectSeeMore(seeMore: self.seeMore)
    }
    
    // MARK: - Public SetContent  -
    public func setContent(author: String, seeMore: String) {
        self.authorLabel.text = author
        self.seeMore = seeMore
    }
    
}
