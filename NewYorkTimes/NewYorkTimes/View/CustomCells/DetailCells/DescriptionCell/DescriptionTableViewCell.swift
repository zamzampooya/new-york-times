//
//  DescriptionTableViewCell.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets  -
    @IBOutlet weak var DescriptionLabel: UILabel!
    
    // MARK: - overrides -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Public SetContent  -
    public func setContent(description: String) {
        self.DescriptionLabel.text = description
    }
}
