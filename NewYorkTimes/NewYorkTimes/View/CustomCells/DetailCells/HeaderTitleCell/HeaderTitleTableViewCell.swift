//
//  HeaderTitleTableViewCell.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import UIKit

class HeaderTitleTableViewCell: UITableViewHeaderFooterView {
    // MARK: - IBOutlet  -
    @IBOutlet weak var topStoryDetailHeader: UILabel!
    
    // MARK: - override  -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Public SetContent  -
    func setContent(headerTitle: String) {
        self.topStoryDetailHeader.text = headerTitle
    }
    
}
