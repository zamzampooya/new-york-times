//
//  TopStoriesTableViewCell.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import UIKit

class TopStoriesTableViewCell: UITableViewCell {

    // MARK: - IBOutlets  -
    @IBOutlet weak var smallImageView: UIImageView! {
        didSet {
            self.smallImageView.layer.cornerRadius = self.smallImageView.frame.width/2
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    // MARK: - Overrides -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Public SetContent  -
    public func setContent(topStoryViewModel: TopStoryViewModel) {
        self.smallImageView.setImage(urlString: topStoryViewModel.smallImage)
        self.titleLabel.text = topStoryViewModel.title ?? ""
        self.authorLabel.text = topStoryViewModel.author ?? ""
    }
}
