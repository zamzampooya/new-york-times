//
//  TopStoryConfigurator.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import UIKit

class TopStoryConfigurator {
    /**
     This method creats the **TopStoryDetailViewController** and set its initial data which is of kind **TopStoryViewModel**.
     
     - Parameter data: the initial data for **TopStoryDetailViewController** which is of kind **TopStoryViewModel**.
     - returns: an istance of **TopStoryDetailViewController**.
       */
    static func createTopStoryDetailViewController(withData data: TopStoryViewModel) -> TopStoryDetailViewController {
        let topStoryDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TopStoryDetailViewController") as? TopStoryDetailViewController
        guard let controller = topStoryDetailViewController else {
            fatalError("could not make TopStoryDetailViewController")
        }
        controller.setContent(topStoryViewModel: data)
        return controller
    }
    
    /**
     This method creats the **SeeMoreWebViewViewController** and set its initial data which is of kind **String**.
     
     - Parameter data: the initial data for **SeeMoreWebViewViewController** which is of kind **String**.
     - returns: an istance of **SeeMoreWebViewViewController**.
     */
    static func createSeeMoreWebViewViewController(withData data: String) -> SeeMoreWebViewViewController {
        let seeMoreWebViewViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SeeMoreWebViewViewController") as? SeeMoreWebViewViewController
        guard let controller = seeMoreWebViewViewController else {
            fatalError("could not make SeeMoreWebViewViewController")
        }
        controller.setContent(seeMore: data)
        return controller
    }
    
}
