//
//  TopStoryViewModel.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation
/**
    This is the ViewModel which is used both is **TopStoriesViewController** and **TopStoryDetailViewController**.
 
    ## Important Notes ##
    1. This class needs a **topStoryModel** for init.
    2. Some variables such as **title** just return the title of **topStoryModel**.
    3. Some Variables such as **smallImage** fetch the data from **topStoryModel**.
 
 */
class TopStoryViewModel {
    var topStoryModel: TopStoryModel
    
    // MARK: - Top Stories Page Data  -
    var smallImage: String? {
        var smallImageUrl: String?
        for multimedia in (self.topStoryModel.multimedia ?? []) where multimedia.format == "thumbLarge" {
            
            smallImageUrl = multimedia.url
            
        }
        return smallImageUrl
    }
    var title: String? {
        return self.topStoryModel.title
    }
    var author: String? {
        return self.topStoryModel.byline
    }
    
    // MARK: - Detail Page Data  -
    var navigationBarTitle: String? {
        return self.topStoryModel.subsection
    }
    var largeImage: String? {
        var largeImageUrl: String?
        for multimedia in (self.topStoryModel.multimedia ?? []) where multimedia.format == "mediumThreeByTwo210" {
            
            largeImageUrl = multimedia.url
            
        }
        return largeImageUrl
    }
    var description: String? {
        return self.topStoryModel.abstract
    }
    var seeMore: String? {
        return self.topStoryModel.url
    }
    // MARK: - init  -
    init(topStoryModel: TopStoryModel) {
        self.topStoryModel = topStoryModel
    }
}
