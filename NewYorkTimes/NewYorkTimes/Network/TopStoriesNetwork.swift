//
//  TopStoriesNetwork.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireCodable
/**
 This class has the duty to fetch the data from server using Alamofire.
 
 ## Important Notes ##
 1. **baseUrl** is saved in **info.plist**.
 2. **apiKey** is saved in **info.plist**.
 */
class TopStoriesNetwork {
    var baseUrl: String {
        guard let stringURL = Bundle.main.infoDictionary?["BackendUrl"] as? String else {
            fatalError("no BackendUrl")
        }
        return stringURL
    }
    var apiKey: String {
        guard let stringURL = Bundle.main.infoDictionary?["APIKey"] as? String else {
            fatalError("no APIKey")
        }
        return stringURL
    }
    
    /**
     This method fetches the data from server and then using **AlamofireCodable** it decodes the data to **TopStoriesModel**.
     - parameter completion: A closure which is called with an array of **TopStoriesModel** objects.
     */
    func getTopStories(completion: @escaping (TopStoriesModel?) -> Void) {
        Alamofire.request(baseUrl, method: .get, parameters: ["api-key": self.apiKey], encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<TopStoriesModel>) in
            switch response.result {
            case .success(let object):
                debugPrint("success")
                completion(object)
            case .failure(let error):
                debugPrint(error)
                debugPrint("failure")
            }
        }
    }
}
