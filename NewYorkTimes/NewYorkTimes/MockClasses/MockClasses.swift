//
//  MockClasses.swift
//  NewYorkTimes
//
//  Created by Zamzam Pooya on 10/18/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import Foundation

class TopStoriesNetworkMock: TopStoriesNetwork {
    private let topStoriesModel: TopStoriesModel
    init(TopStoriesModel: TopStoriesModel) {
        self.topStoriesModel = TopStoriesModel
    }
    override func getTopStories(completion: @escaping (TopStoriesModel?) -> Void) {
        completion(topStoriesModel)
    }
    
}


class TopStoriesViewMock: TopStoriesPresenterInterface {
    var setTopStoriesCalled = false
    var handleErrorCalled = false

    func setTopStories(topStories: [TopStoryViewModel]) {
        self.setTopStoriesCalled = true
    }
    
    func handleError() {
        self.handleErrorCalled = true
    }
    
    func startLoading() {
    }
    
    func stopLoading() {
    }
    
}
