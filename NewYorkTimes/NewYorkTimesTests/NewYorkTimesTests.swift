//
//  NewYorkTimesTests.swift
//  NewYorkTimesTests
//
//  Created by Zamzam Pooya on 10/17/18.
//  Copyright © 2018 ZDevelop. All rights reserved.
//

import XCTest
@testable import NewYorkTimes

class NewYorkTimesTests: XCTestCase {
    let handleErrorNetworkMock = TopStoriesNetworkMock(TopStoriesModel: TopStoriesModel())
    let setTopStoriesNetwork = TopStoriesNetwork()
    func testHandleError() {
        let topStoriesViewMock = TopStoriesViewMock()
        let topStoriesPresenterUnderTest = TopStoriesPresenter(delegate: topStoriesViewMock, network: handleErrorNetworkMock)
        
        topStoriesPresenterUnderTest.getTopStories()
        
        XCTAssertTrue(topStoriesViewMock.handleErrorCalled)
    }
    
    func testSetTopStories() {
        let exp = self.expectation(description: "not nil Data")

        let topStoriesViewMock = TopStoriesViewMock()
        let topStoriesPresenterUnderTest = TopStoriesPresenter(delegate: topStoriesViewMock, network: setTopStoriesNetwork)
        
        topStoriesPresenterUnderTest.network.getTopStories { (topStoriesModel) in
            if topStoriesModel?.results?.count ?? 0 > 0 {
                exp.fulfill()
            }
        }
        
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
       
        
    }
    
}
