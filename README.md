# New York Times

## Description
This project is an iOS challenge. It shows a list of ​New York Times​ **​Top Stories​​** with a detail view. The design pattern used in this project is **MVP**. 

## Getting Started
In order to run the project, clone the project and run it.

## Requirements
- iOS 10.0+
- Xcode 10.0+
- Swift 4.2+

## Author
@zamzampooya, zamzampooya@gmail.com

